from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

class Cliente(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    rut = models.CharField('Rut', max_length=12, null=True)
    nombre = models.CharField('Nombre', max_length=50, null=True)
    email = models.EmailField('Email', max_length=50, null=True)
    fecha_nacimiento = models.DateField('Fecha Nacimiento', auto_now_add=True)
    telefono_contacto = models.CharField('Telefono', max_length=50, null=True)
    region = models.CharField('Region', max_length=50, null=True)
    comuna = models.CharField('Comuna', max_length=50, null=True)
  
    def __str__(self):
        return self.rut


class Servicio(models.Model):
    id_cliente = models.ForeignKey(Cliente, null=True, on_delete=models.CASCADE)
    Tipo_servicio = (
        ('Plan Estandar', 'Plan Estandar'),
        ('Plan Premium', 'Plan Premium'),
        ('Tax Master', 'TaxMaster'),
    )
    precio = models.IntegerField('costo', null=True)
    tipo = models.CharField('tipo', null=True, max_length=20, choices=Tipo_servicio)

    def __str__(self):
        return self.tipo

