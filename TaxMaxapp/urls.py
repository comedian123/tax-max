from django.conf.urls import url
from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

app_name = 'gestion'

urlpatterns = [    
    path('', views.Main, name='Main'),
    path('Nosotros/', views.Nosotros, name='Nosotros'),
    path('Main/', views.Main, name='Main'),
    path('Servicio/', views.Servicio, name='Servicio'),
    path('Registro/', views.Registrar, name='Registro'),
    path('Login/', views.Usuario_login, name='Login'),
    path('Logout/', views.Usuario_logout, name='Logout'),
    path('Contacto/', views.Contacto, name='Contacto'),    
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)



















