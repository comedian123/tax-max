# Generated by Django 2.2.6 on 2019-11-03 01:12

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Cliente',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('rut', models.CharField(max_length=12, null=True, verbose_name='Rut')),
                ('nombre', models.CharField(max_length=50, null=True, verbose_name='Nombre')),
                ('email', models.EmailField(max_length=50, null=True, verbose_name='Email')),
                ('fecha_nacimiento', models.DateField(auto_now_add=True, verbose_name='Fecha Nacimiento')),
                ('telefono_contacto', models.CharField(max_length=50, null=True, verbose_name='Telefono')),
                ('region', models.CharField(max_length=50, null=True, verbose_name='Region')),
                ('comuna', models.CharField(max_length=50, null=True, verbose_name='Comuna')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Servicio',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('precio', models.IntegerField(null=True, verbose_name='costo')),
                ('tipo', models.CharField(choices=[('Plan Estandar', 'Plan Estandar'), ('Plan Premium', 'Plan Premium'), ('Tax Master', 'TaxMaster')], max_length=20, null=True, verbose_name='tipo')),
                ('id_cliente', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='TaxMaxapp.Cliente')),
            ],
        ),
    ]
