from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from .models import Cliente, Servicio
from django import forms


class PerfilUsuarioForm(forms.ModelForm):
    class Meta():
        model = Cliente
        fields=[
            'rut',                    
            'telefono_contacto',
            'region',
            'comuna'
        ]

        labels={            
            'rut':'Rut valido',            
            'telefono_contacto':'Celular',
            'region':'Region',
            'comuna':'Comuna'
        }  


class RegistroForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    class Meta():
        model = User
        fields=[
            'username',
            'email',
            'password'
            
        ]
        labels={
            'username':'Nombre de usuario',
            'email':'Correo',
            'password':'Contraseña'
            

        }
        help_texts={
            'username':'Usuario AQUI!!',
            'email':'Ingrese correo AQUI!!',
            'password':'Contraseña AQUI!!'
            
        }
        error_messages ={
            'username':{
                'max_length':'Maximo 20 caracteres',
                'min_length':'Minimo 8 caracteres',
                'required':'Requerido'
            },
            'email':{
                'required':'Requerido'
            },
            'password':{
                'min_length':'Minimo 8 caracteres',
                'required':'Requerido'
            }
            

        }

    def __init__(self, *args, **kwargs):
        super(RegistroForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs.update({'class': 'forms-control'})
        self.fields['email'].widget.attrs.update({'class': 'forms-control'})
        self.fields['password'].widget.attrs.update({'class': 'forms-control'})
        

