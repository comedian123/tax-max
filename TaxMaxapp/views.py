from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django import forms
from .models import Cliente,Servicio
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from .forms import RegistroForm, PerfilUsuarioForm


def Servicio(request):

    return render(request,'gestion/Servicio.html') 

def Main(request):

    return render(request, 'gestion/Main.html')

def Nosotros(request):

    return render(request,'gestion/Nosotros.html')    

def Contacto(request):

    return render(request,'gestion/Contacto.html')      
    
def Usuario_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('gestion:Main'))

def Index(request):
    return render(request, 'gestion/Main.html')    


def Usuario_login(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect(reverse('gestion:Main'))
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(username=username, password=password)
            if user:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect(reverse('gestion:Main'))
                else:
                    return HttpResponse("Tu cuenta está inactiva    UwU")      
            else: 
                print("username: {} - password: {}".format(username, password))
                return HttpResponse("Datos Inválidos")
        else:
            return render(request, 'gestion/Login.html',{})        


def Registrar(request):
    registrado= False
    if request.method == 'POST':
        user_form = RegistroForm(data=request.POST)
        profile_form = PerfilUsuarioForm(data=request.POST)
        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            profile = profile_form.save(commit=False)
            profile.user = user
            profile.save()
            registrado = True
        else:       
            print(user_form.errors, profile_form.errors)     
            return HttpResponse("Datos Invalidos")    
    else:
        user_form = RegistroForm()     
        profile_form = PerfilUsuarioForm()

    return render(request, 'gestion/Registro.html',
                     {'user_form': user_form,
                     'profile_form': profile_form,
                     'registrado': registrado})



# ---importamos nuestras estruras ---------------


# Create your views here.

