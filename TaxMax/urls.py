
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
# Este es el "directorio" de las rutas generales!!!
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('TaxMaxapp.urls')),
    path('Nosotros/', include('TaxMaxapp.urls')),
    path('Servicio/', include('TaxMaxapp.urls')),    
    path('Registro/', include('TaxMaxapp.urls')),
    path('Login/', include('TaxMaxapp.urls')),
    path('Logout/', include('TaxMaxapp.urls')),
    path('Contacto/', include('TaxMaxapp.urls')),
    path('Main/', include('TaxMaxapp.urls'))    
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
